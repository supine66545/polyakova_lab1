#include <iostream>
#include <math.h>

using namespace std;

template<typename T>
void calc_func(T a, T b)
{
    T sol, s1, s2, s3, s4;
    s1 = pow(a-b, 3);
    s2 = pow(a, 3) - 3 * a * a * b;
    s3 = s1 - s2;
    s4 = pow(b, 3) - 3 * a * b * b;
    sol = s3 / s4;

    if (sizeof(a) == 4) cout << "float  | ";
    else cout << "double | ";

    cout.setf(ios::fixed);
    cout << s1 << "\t| " << s2 << "\t| " << s3 << "\t| " << s4 << "\t| " << sol << "\n";

    cout.unsetf(ios::fixed);
}

int main()
{
    float fa=1000, fb=0.0001;
    double da=1000, db=0.0001, m , n;
    char repeat;

    cout << "1. Comparision of results when using float and double:\n";
    calc_func(fa, fb);
    calc_func(da, db);

    do
    {
        cout << "\n2. Enter m and n:\n";
        cin >> m  >> n;
        cout << "\tA. --m - ++n:\t" << --m - ++n << "\n";
        cout << "\tB. m * n < n++:\t" << ((m * n < n++) ? "true" : "false") << "\n";
        cout << "\tC. n-- > m++:\t" << ((n-- > m++) ? "true" : "false") << "\n";

        cout << "Repeat? (Press the '0' to exit): ";
        cin >> repeat;
    } while (repeat != 48);
    return 0;
}
